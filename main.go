package main

import (
	
	"log"
)

var (
    device      string = "usb0"
    snapshotLen int  = 1600
    verbose bool   = false
    filter   string =  "tcp"
)

func main() {
	
	icpr := Interceptor{Iface: device, Filter: filter, Snaplen: snapshotLen, Verbose: verbose}

	log.Println(&icpr)

	icpr.Run()

}
